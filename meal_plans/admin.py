from django.contrib import admin
from meal_plans.models import MealPlan

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    pass

admin.site.register(MealPlan, PostAdmin)

