from sqlite3 import IntegrityError
from django.core.paginator import Paginator
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin


from recipes.forms import RatingForm
from recipes.models import USER_MODEL, Ingredient, Recipe, Shoppingitems


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            try:
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


def get_queryset(self):
    query = self.request.GET.get("q")
    if not query:
        query = ""
    return Recipe.objects.filter(title_icontains=query)


class RecipeListView(LoginRequiredMixin, ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2

    def listing(request):
        recipe_list = Recipe.objects.all()
        paginator = Paginator(recipe_list, 2)

        page_number = request.GET.get("page")
        page_obj = paginator.get_page(page_number)
        return render(request, "list.html", {"page_obj": page_obj})


class RecipeDetailView(LoginRequiredMixin, DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ratings"] = RatingForm()
        # foods = []

        # for item in self.request.user.shoppingitems.all():
        #     foods.append(item.food_item)
        context["servings"] = self.request.GET.get("servings")
        # context["food_in_shoppinglist"] = foods
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


def createshoppingitem(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)
    user = request.user
    try:
        Shoppingitems.objects.create(
            food_item=ingredient.food,
            user=user,
        )
    except IntegrityError:
        pass
    return redirect("recipes_detail", pk=ingredient.recipe.id)


class ShoppingListView(ListView):
    model = Shoppingitems
    template_name = "shoppinglist_list"

    def get_queryset(self):
        return Shoppingitems.objects.filter(user_id=self.request.user)


def delete_all_shopping_items(request):
    Shoppingitems.objects.filter(user=request.user).delete()
    return redirect("shoppinglist_list")
