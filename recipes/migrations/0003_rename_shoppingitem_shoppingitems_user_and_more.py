# Generated by Django 4.0.3 on 2022-05-02 02:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_shoppingitems'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shoppingitems',
            old_name='shoppingitem',
            new_name='user',
        ),
        migrations.AddField(
            model_name='shoppingitems',
            name='food_item',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='recipes.fooditem'),
        ),
    ]
