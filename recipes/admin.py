from django.contrib import admin
from recipes.models import (
    Recipe,
    Measure,
    FoodItem,
    Ingredient,
    Step,
    Rating,
    Shoppingitems,
)

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    pass


admin.site.register(Recipe, PostAdmin)
admin.site.register(Measure, PostAdmin)
admin.site.register(FoodItem, PostAdmin)
admin.site.register(Ingredient, PostAdmin)
admin.site.register(Step, PostAdmin)
admin.site.register(Rating, PostAdmin)
admin.site.register(Shoppingitems, PostAdmin)
