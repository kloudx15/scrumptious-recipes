from django.shortcuts import render
from django.core.paginator import Paginator

# from django.urls import reverse_lazy
from django.views.generic.list import ListView

# from django.views.generic.edit import CreateView, UpdateView, DeleteView
# from django.views.generic.detail import DetailView


from tags.models import Tag


# Create your views here.
class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"

    def listing(request):
        tag_list = Tag.objects.all()
        paginator = Paginator(tag_list, 10)

        page_number = request.GET.get("page")
        page_obj = paginator.get_page(page_number)
        return render(request, "list.html", {"page_obj": page_obj})


# class TagCreateView(CreateView):
#     model = Tag
#     template_name = "tags/new.html"
#     fields = ["name"]
#     success_url = reverse_lazy("tags_list")

# class TagUpdateView(UpdateView):
#     model = Tag
#     template_name = "tags/edit.html"
#     fields = ["name"]
#     success_url = reverse_lazy("tags_list")

# class TagDeleteView(DeleteView):
#     model = Tag
#     template_name = "tags/delete.html"
#     success_url = reverse_lazy("tags_list")

# class TagDetailView(DetailView):
#     model = Tag
#     template_name = "tags/details.html"
