from django.urls import path

from tags.views import (
    TagListView,
    # TagCreateView,
    # TagDeleteView,
    # TagDetailView,
    # TagUpdateView,
)

urlpatterns = [
    path("", TagListView.as_view(), name="tags_list"),
    # path("<new/>", TagCreateView.as_view(), name="tags_new"), 
    # path("<int:pk>/delete/", TagDeleteView.as_view(), name="tags_delete"),
    # path("<int:pk>/", TagDetailView.as_view(), name="tags_details"),
    # path("<int:pk>/edit/", TagUpdateView.as_view(), name="tags_edit"), 
]
