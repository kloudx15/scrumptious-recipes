from django.contrib import admin
from tags.models import Tag

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    pass

admin.site.register(Tag, PostAdmin)